FROM golang:1.16.3-alpine3.13 as build

ENV DOCKER_GEN_VERSION=0.7.6

RUN apk --no-cache add git make musl-dev gcc \
        && mkdir /build \
        && cd /build \
        && git clone https://github.com/jwilder/docker-gen.git \
        && cd docker-gen \
        && git checkout ${DOCKER_GEN_VERSION} \
        && make

FROM alpine:3.13
RUN apk -U add openssl
ENV DOCKER_HOST=unix:///tmp/docker.sock
COPY --from=build /build/docker-gen/docker-gen /usr/local/bin/docker-gen
COPY nginx.tmpl /etc/docker-gen/templates/nginx.tmpl
ENTRYPOINT ["/usr/local/bin/docker-gen"]
